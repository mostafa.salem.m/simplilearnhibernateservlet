package com.control;
import com.javatpoint.*;


import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.Session;

public class hibernatemain {

	public static void main(String[] args) {
		System.out.println("HEllo");
		Configuration configuration =new Configuration().configure();
		configuration.addAnnotatedClass(com.javatpoint.Employee.class);
		StandardServiceRegistryBuilder builder= 
				new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());

		SessionFactory factory= configuration.buildSessionFactory(builder.build());
		Session session=factory.openSession();
		Transaction transaction=session.beginTransaction();
		Employee e1=new Employee(12,"Mostafa","Salem");
		session.save(e1);
		transaction.commit();
		session.close();





	}

}
